﻿namespace GameScore.Configurations;

/// <summary>
/// The database settings for the player. 
/// </summary>
public class PlayerDatabaseSettings
{
    /// <summary>
    /// Connection string
    /// </summary>
    public string ConnectionString { get; set; } = string.Empty;

    /// <summary>
    /// DB Name
    /// </summary>
    public string DatabaseName { get; set; } = string.Empty;
    /// <summary>
    /// Collection Name
    /// </summary>
    public string PlayerCollectionName { get; set; } = string.Empty;
    /// <summary>
    /// How many results per top scoring player page should return
    /// </summary>
    public int ResultPerPage { get; set; } = 10;
    /// <summary>
    /// Used to increase JWT token strength. Not really part of the database but pepper is used to hash passwords saved to the database
    /// </summary>
    public string Pepper { get; set; } = string.Empty;

}
