﻿namespace GameScore.Configurations;

/// <summary>
/// Settings for the distributed caching. Values are mapped from the appsettings.
/// </summary>
public class DistCacheSettings
{
    /// <summary>
    /// IP/domain of the Redis/Ncache/Sql/Memory cache server
    /// </summary>
    public string Location { get; set; } = string.Empty;
    /// <summary>
    /// Password to connect to the cache server
    /// </summary>
    public string Password { get; set; } = string.Empty;
    /// <summary>
    /// How long should the value be kept in the cache?
    /// </summary>
    public string ExpireDuration { get; set; } = string.Empty;
    /// <summary>
    /// How many pages of the leaderboard (from top to bottom) should be cached?
    /// </summary>
    public int MaxCachedLeaderPage { get; set; } = 2;
}
