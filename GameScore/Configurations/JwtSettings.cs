﻿namespace GameScore.Configurations;

/// <summary>
/// JWT creation settings. Values are mapped from appsettings
/// </summary>
public class JwtSettings
{
    /// <summary>
    /// Issuer of the token.
    /// </summary>
    public string Issuer { get; set; } = string.Empty;
    /// <summary>
    /// Audience of the token.
    /// </summary>
    public string Audience { get; set; } = string.Empty;
    /// <summary>
    /// Key of the token. Alternatively, key could be read from another file/registry/DB etc.
    /// </summary>
    public string Key { get; set; } = string.Empty;
    /// <summary>
    /// ExpireDuration of the token.
    /// </summary>
    public string ExpireDuration { get; set; } = string.Empty;
}