# GameScore service. 
A Rest API service to keep track of the player game scores and query the global leaderboard page by page.

## Quick Start Notes:
### Prerequisites

1. [Visual Studio 2022](https://visualstudio.microsoft.com/vs/) with .net 6 
2. [Docker](https://docs.docker.com/get-docker/)

### Running the program

1. Start the database. From the command line, change directory into \Docker\MongoDb and run <blockquote>docker compose up</blockquote>
2. Start distributed cache server. From the command line, change directory into \Docker\Redis and run <blockquote>docker compose up</blockquote>
3. Open the solution, build and publish or run the application from the IDE.

Swagger with authentication function is enabled. Any modern web browser can be used as a client.