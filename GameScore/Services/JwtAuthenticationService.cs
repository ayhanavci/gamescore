﻿using GameScore.Configurations;
using GameScore.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
namespace GameScore.Services;

/// <summary>
/// Implementation of authentication interface. Used by the AuthController
/// </summary>
public class JwtAuthenticationService : IAuthenticationService
{    
    private readonly IPlayerService _playerService;
    private readonly IOptions<PlayerDatabaseSettings> _playerDatabaseSettings;
    private readonly IOptions<JwtSettings> _jwtSettings;

    /// <summary>
    /// Constructor. The parameters are for accessing the settings and to read and compare the player password from the database.
    /// </summary>
    /// <param name="playerService">Data operations. Internally decides between the cache or the database</param>
    /// <param name="playerDatabaseSettings">Just used to read the pepper here</param>
    /// <param name="jwtSettings">The vendor information to generate a JWT token. This maps to JWT in the appsettings</param>
    public JwtAuthenticationService(     
        IPlayerService playerService,
        IOptions<PlayerDatabaseSettings> playerDatabaseSettings,
        IOptions<JwtSettings> jwtSettings)
    {        
        _playerService = playerService;
        _playerDatabaseSettings = playerDatabaseSettings;
        _jwtSettings = jwtSettings;
    }
    /// <summary>
    /// Checks if the password is valid. Creates and returns the JWT token.
    /// </summary>
    /// <param name="user">Player name</param>
    /// <returns>JWT Token</returns>
    public async Task<string?> Authenticate(LoginUserDto user)
    {
        var player = await _playerService.GetPlayerByNameAsync(user.UserName);
        if (player == null) return string.Empty;

        if (HashPassword(user.Password, player.Salt, _playerDatabaseSettings.Value.Pepper).SequenceEqual(player.Password))
            return CreateJwtToken(user.UserName);
            
        return string.Empty;                
    }
    /// <summary>
    /// Helper function to create a new JWT token using the parameters from appsettings
    /// </summary>
    /// <param name="userName">Player name</param>
    /// <returns>Jwt token</returns>
    private string CreateJwtToken(string userName)
    {
        List<Claim> claims = new()
        {
            new Claim("Id", Guid.NewGuid().ToString()),
            new Claim(ClaimTypes.Name, userName),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
        };

        var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Value.Key));
        var credentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha512Signature);
        var tokenOptions = new JwtSecurityToken(
               issuer: _jwtSettings.Value.Issuer,
               audience: _jwtSettings.Value.Audience,
               claims: claims,               
               expires: DateTime.Now.Add(TimeSpan.Parse(_jwtSettings.Value.ExpireDuration)),
               signingCredentials: credentials
           );

        return new JwtSecurityTokenHandler().WriteToken(tokenOptions);
    }
    /// <summary>
    /// Hashes player password with salt and pepper. 
    /// </summary>
    /// <param name="password">Player password in plain format</param>
    /// <param name="salt">Random Salt (saved in db)</param>
    /// <param name="pepper">Static Pepper (saved in appsettings)</param>
    /// <returns>Hashed password with salt and pepper</returns>
    public string HashPassword(string password, string salt, string pepper)
    {
        SHA256 hash = SHA256.Create();
        var passwordBytes = Encoding.Default.GetBytes($"{password}{salt}{pepper}");
        var hashedPassword = hash.ComputeHash(passwordBytes);
      
        return Convert.ToHexString(hashedPassword);
    }
    /// <summary>
    /// Generates a salt to be used for hashing the password
    /// </summary>
    /// <param name="nSalt">The number of size in bytes of the random Salt</param>
    /// <returns>Random salt</returns>
    public string GenerateSalt(int nSalt)
    {
        var saltBytes = new byte[nSalt];                
        using (var provider = RandomNumberGenerator.Create())
        {
            provider.GetNonZeroBytes(saltBytes);
        }       
        return Convert.ToBase64String(saltBytes);
    }    

}
