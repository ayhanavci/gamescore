﻿using GameScore.Configurations;
using GameScore.Models;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Text.Json;

namespace GameScore.Services;

/// <summary>
/// Implementation for database Interface. Performs DB operations and uses caching. 
/// </summary>
/// <remarks>
/// Caching is done using IDistributedCache interface. This is a generic interface and can use In memory cache, SQL server cache, Redis cache and Ncache
/// with the same interface without having to change the code. The decision is made in program.cs
/// </remarks>
public class MongoPlayerService : IPlayerService
{
	private readonly IMongoCollection<Player> _players;	
    private readonly IDistributedCache _cache;
    private readonly IOptions<PlayerDatabaseSettings> _playerDatabaseSettings;
    private readonly IOptions<DistCacheSettings> _distCacheSettings;

    /// <summary>
    /// Constructor. Uses app settings and platform agnostic caching interface.
    /// </summary>
    /// <param name="cache"></param>
    /// <param name="playerDatabaseSettings"></param>
    /// <param name="distCacheSettings"></param>
    public MongoPlayerService(
        IDistributedCache cache,
        IOptions<PlayerDatabaseSettings> playerDatabaseSettings,
        IOptions<DistCacheSettings> distCacheSettings)
	{
        _cache = cache;
        _playerDatabaseSettings = playerDatabaseSettings;
        _distCacheSettings = distCacheSettings;
        
        var mongoClient = new MongoClient(_playerDatabaseSettings.Value.ConnectionString);
		var mongoDatabase = mongoClient.GetDatabase(_playerDatabaseSettings.Value.DatabaseName);        
        _players = mongoDatabase.GetCollection<Player>(_playerDatabaseSettings.Value.PlayerCollectionName);
		CreateScoreIndex();
        CreateUserNameIndex();
    }
    /// <summary>
    /// Helper function to create database sort index for score to query faster.
    /// </summary>    
	private void CreateScoreIndex()
	{
        var keys = Builders<Player>.IndexKeys.Descending("Score");
        var indexOptions = new CreateIndexOptions { Unique = false };
        var model = new CreateIndexModel<Player>(keys, indexOptions);
        _players.Indexes.CreateOne(model);
    }
    /// <summary>
    /// Helper function to create database unique index to the user name.
    /// </summary>
    /// <remarks>
    /// Player name is marked as unique to prevent possible sync issues if different instances of the application runs.
    /// </remarks>
    private void CreateUserNameIndex()
    {
        var keys = Builders<Player>.IndexKeys.Ascending("Name");
        var indexOptions = new CreateIndexOptions { Unique = true };
        var model = new CreateIndexModel<Player>(keys, indexOptions);
        _players.Indexes.CreateOne(model);        
    }
    /// <summary>
    /// Reads the leaderboard from the cache or the database
    /// </summary>
    /// <param name="page">Page of the leaderboard, index starts from 0. Player per page can be changed from appsettings ResultPerPage. And max cached page count can be changed from MaxCachedLeaderPage</param>
    /// <returns>Leaderboard page</returns>
    public async Task<List<Player>?> GetAllPlayersByScoreDescAsync(int page)
    {
        if (page > _distCacheSettings.Value.MaxCachedLeaderPage) //Leaderboard pages have a limit. Could be made unlimited
            return null;

        //Try to return cached value
        List<Player>? playerList = GetCachedLeaderBoard(page);        
        if (playerList != null) return playerList;
        
        //Cached page not found or cached data is corrupt. Read from DB.
        playerList = await _players
                .Find(_ => true)
                .SortByDescending(p => p.Score)                
                .Limit(Convert.ToInt32(_playerDatabaseSettings.Value.ResultPerPage))
                .Skip(page * Convert.ToInt32(_playerDatabaseSettings.Value.ResultPerPage))
                .ToListAsync();

        //Save to cache
        if (playerList != null)
        {
            var serializedList = JsonSerializer.Serialize(playerList);
            var options = new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = TimeSpan.Parse(_distCacheSettings.Value.ExpireDuration) };
            _cache.SetString($"ScoreFirstPage{page}", serializedList, options);
        }
        
        return playerList;
        
    }
    /// <summary>
    /// Helper function to attempt to read the cached leader board page if any.
    /// </summary>
    /// <param name="page">Page of the leaderboard, index starts from 0. Player per page can be changed from appsettings ResultPerPage. And max cached page count can be changed from MaxCachedLeaderPage</param>
    /// <returns>Leaderboard page</returns>
    private List<Player>? GetCachedLeaderBoard(int page)
    {
        string? cachedValue = _cache.GetString($"ScoreFirstPage{page}");
        if (cachedValue != null)
        {
            var options = new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = TimeSpan.Parse(_distCacheSettings.Value.ExpireDuration) };
            _cache.SetString($"ScoreFirstPage{page}", cachedValue, options); //Update expiry time            
            return JsonSerializer.Deserialize<List<Player>>(cachedValue);
        }
        return null;
    }
    /// <summary>
    /// Returns all players. Unused.
    /// </summary>
    /// <returns></returns>
    public async Task<List<Player>?> GetAllPlayersAsync() => await _players.Find(_ => true).ToListAsync();
   
    /// <summary>
    /// Reads player record from the cache or the database
    /// </summary>
    /// <param name="name">Player name to query</param>
    /// <returns>Player record</returns>
    public async Task<Player?> GetPlayerByNameAsync(string name)
    {
        Player? player;
        //Try to return cached value
        string? cachedValue = _cache.GetString(name);
        if (cachedValue != null)
        {
            player = JsonSerializer.Deserialize<Player>(cachedValue);
            var options = new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = TimeSpan.Parse(_distCacheSettings.Value.ExpireDuration) };
            _cache.SetString(name, cachedValue, options); //Update expiry time
            if (player != null) return player;
        }

        //Cached player not found. Get it from db
        player = await _players.Find(x => x.Name == name).FirstOrDefaultAsync();

        //Cache player
        if (player != null)
        {
            var serializedPlayer = JsonSerializer.Serialize(player);
            var options = new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = TimeSpan.Parse(_distCacheSettings.Value.ExpireDuration) };
            _cache.SetString(name, serializedPlayer, options);
        }        
        return player;
    }
    /// <summary>
    /// Creates a new player in the database and caches it.
    /// </summary>
    /// <param name="newPlayer">Player name to create</param>
    /// <returns></returns>
    public async Task CreatePlayerAsync(Player newPlayer)
    {
        await _players.InsertOneAsync(newPlayer);

        if (!string.IsNullOrEmpty(newPlayer.Id))
        {
            var serializedPlayer = JsonSerializer.Serialize(newPlayer);
            var options = new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = TimeSpan.Parse(_distCacheSettings.Value.ExpireDuration) };
            _cache.SetString(newPlayer.Name, serializedPlayer, options);
        }
    }
    /// <summary>
    /// Updates player score and re-caches it. Also clears cached leaderboard.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="updatedPlayer">Player id and data to update</param>
    /// <returns></returns>
    public async Task UpdatePlayerAsync(string id, Player updatedPlayer)
    {
        await _players.ReplaceOneAsync(x => x.Id == id, updatedPlayer);
        
        var serializedPlayer = JsonSerializer.Serialize(updatedPlayer);
        var options = new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = TimeSpan.Parse(_distCacheSettings.Value.ExpireDuration) };
        _cache.SetString(updatedPlayer.Name, serializedPlayer, options);

        //Clear cached leaderboard. Better approach would be finding the new placement of the player score in the leaderboard and then only invalidate the lower ones.
        for (int page = 0; page < _distCacheSettings.Value.MaxCachedLeaderPage; ++page)
            _cache.Remove($"ScoreFirstPage{page}");
    }
    /// <summary>
    /// Removes a player by name
    /// </summary>
    /// <param name="name">Player name to remove</param>
    /// <returns></returns>
    public async Task RemovePlayerAsync(string name)
    {
        await _players.DeleteOneAsync(x => x.Name == name);
        _cache.Remove(name);
    }
            
}
