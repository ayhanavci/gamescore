﻿using GameScore.Models;

namespace GameScore.Services;
/// <summary>
/// Interface for the authentication service. Dependency injected in program.cs
/// </summary>
public interface IAuthenticationService
{
    /// <summary>
    /// Authenticates the user.
    /// </summary>
    /// <param name="user">Player name</param>
    /// <returns></returns>
    public Task<string?> Authenticate(LoginUserDto user);
    /// <summary>
    /// Hashes the password with salt and pepper to save into the database.
    /// </summary>
    /// <param name="plainPassword">Player password in plain format</param>
    /// <param name="salt">Random Salt (saved in db)</param>
    /// <param name="pepper">Static Pepper (saved in appsettings)</param>
    /// <returns></returns>
    public string HashPassword(string plainPassword, string salt, string pepper);
    /// <summary>
    /// Generates a random salt for each user
    /// </summary>
    /// <param name="nSalt">The number of size in bytes of the random Salt</param>
    /// <returns></returns>
    public string GenerateSalt(int nSalt);
}
