﻿using GameScore.Models;

namespace GameScore.Services;

/// <summary>
/// Interface for the query service. Dependency injected in program.cs
/// </summary>
public interface IPlayerService
{
    /// <summary>
    /// Returns all players in the database. 
    /// </summary>
    /// <remarks>
    /// This method is unusued.
    /// </remarks>
    /// <returns>List of all the players or null</returns>
    public Task<List<Player>?> GetAllPlayersAsync();        
    /// <summary>
    /// Queries a specific user
    /// </summary>
    /// <param name="name">Player name</param>
    /// <returns>The player data or null</returns>
    public Task<Player?> GetPlayerByNameAsync(string name);
    /// <summary>
    /// Gets top scorers leaderboard page by page in a descending order. The max page count and player per page is in the settings.
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public Task<List<Player>?> GetAllPlayersByScoreDescAsync(int page);
    /// <summary>
    /// Registeration of a new unique user. The Auth service checks uniqueness but also there is a unique index on the db.
    /// </summary>
    /// <param name="newPlayer"></param>
    /// <returns></returns>
    public Task CreatePlayerAsync(Player newPlayer);
    /// <summary>
    /// Updates the player information. Only score is used for now.
    /// </summary>
    /// <param name="id">Database id of the player</param>
    /// <param name="updatedPlayer">Updated player details</param>
    /// <returns></returns>
    public Task UpdatePlayerAsync(string id, Player updatedPlayer);
    /// <summary>
    /// Deleting a player's record in the database
    /// </summary>
    /// <param name="name">Player name</param>
    /// <returns></returns>
    public Task RemovePlayerAsync(string name);        
    
}
