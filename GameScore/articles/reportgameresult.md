﻿# Report New Score

The below diagram illustrates a successfull player score submission. This method requires authentication and reads the JWT token from the http header. New player data is updated in the database. 
Then the player is re-cached and top scorer tables are cleared. (This last part could change). The update could be done blindly into the database since the player name is unique. But player's existence is first confirmed.

![alt text](../images/reportgameresult.png)