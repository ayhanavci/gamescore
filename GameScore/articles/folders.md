﻿# Folder Structure

```
Root
│   GameScore.sln
│   
└───GameScore <-- Project folder
│
└───Documentation
│   │   index.html <-- This documentation
│   │
│   
└───Docker
    │   
    └───MongoDb   
    │   │   docker-compose.yml <-- Database compose. 
    │    
    └───Redis   
        │   docker-compose.yml <-- Distributed cache compose. 
```

