﻿# Registration

The below diagram illustrates a successfull player registration. This assumes the player is not previously registered. To check if the player already exists, the Player Service tries to find the player in cache. 
If it doesn't exist in the cache then the database is queried. If any of those return that the player has already registered, then Web Api stops checking and returns 400 Bad Request "Player already exists". 

![alt text](../images/registration.png)