﻿# Get Leaderboard

The below diagram illustrates a successfull leader board page retrieval. Player service tries to retrieve the relevant page from the cache, otherwise from the database. If it gets it from the cache, the results are directly returned.
If page doesn't exist in the cache, then database query is performed and leaderboard page is cached. The results are limited for easier test purposes. Player count per page can be set from appsettings ResultPerPage.
And the limit to number of pages is MaxCachedLeaderPage. This last one was added to artificially limit the caching, which is not really necessary.

![alt text](../images/getleaderboard.png)