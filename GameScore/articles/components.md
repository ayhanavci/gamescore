# Components

* Game client is any client which can consume web api.
* Game Score Wep Api hosts REST Api and does all the processing.
* Mongo DB is the application database which holds the player data. Runs on a docker container.
* Redis is used as for distributed caching. Runs on a docker container.

![alt text](../images/components.png)

