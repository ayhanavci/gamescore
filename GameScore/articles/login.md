﻿# Login

The below diagram illustrates a successfull player login. This assumes the player is previously registered. To check if the player already exists, the Player Service tries to find the player in cache. 
If it doesn't exist in the cache then the database is queried. If any of those return that the player doesn't exist, then Web Api stops checking and returns 401 Unauthorized. A successfull login sequence returns a JWT token to the client.
The JWT token and caching expire durations are set in the appsettings. Jwt epxpire is set to 1 minute, and each cached item expires in 30 seconds for testing purposes. 

![alt text](../images/login.png)