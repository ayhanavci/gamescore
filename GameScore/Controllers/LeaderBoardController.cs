﻿using GameScore.Configurations;
using GameScore.Models;
using GameScore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace GameScore.Controllers;

/// <summary>
/// Submits user scores and returns leader board
/// </summary>
[Route("api/[controller]")]
[ApiController]
public class LeaderBoardController : ControllerBase
{        
    private readonly IPlayerService _playerService;

    /// <summary>
    /// Constructor. Parameters come from the dependency injection.
    /// </summary>    
    /// <param name="playerService">ata operations. Internally decides between the cache or the database</param>    
    public LeaderBoardController(IPlayerService playerService)
    {                
        _playerService = playerService;        
    }

    /// <summary>
    /// Submits the player score.The only api that requires authentication.
    /// </summary>
    /// <param name="score">Submitted player score</param>
    /// <returns>User name and new score</returns>
    [HttpPost]        
    [Route("ReportGameResult")]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PlayerScoreDto))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public async Task<IActionResult> ReportGameResult(int score)
    {        
        string? userName = User.Claims?.FirstOrDefault(claim => claim.Type == ClaimTypes.Name)?.Value;
        if (userName == null) return Unauthorized("Invalid Token");

        var player = await _playerService.GetPlayerByNameAsync(userName);
        if (player == null) return NotFound("Player not found");

        player.Score = score;
        await _playerService.UpdatePlayerAsync(player.Id, player);

        return Ok(new PlayerScoreDto { Name = player.Name, Score = score }) ;
    }

    /// <summary>
    /// Gets top scorers, desc sorted page by page (2 per page for testing)
    /// </summary>
    /// <param name="page">Page of the leaderboard, index starts from 0. Player per page can be changed from appsettings ResultPerPage. And max cached page count can be changed from MaxCachedLeaderPage</param>
    /// <returns>Sorted leader board page</returns>
    [HttpGet]
    [Route("GetLeaderBoard")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<PlayerScoreDto>))]
    public async Task<IActionResult> GetLeaderBoard(int page)
    {
        var players = await _playerService.GetAllPlayersByScoreDescAsync(page);
        var playerScores = from p in players select new PlayerScoreDto()
                           {                                             
                                Name = p.Name,
                                Score = p.Score,
                           };
        return Ok(playerScores);
    }           
}
