﻿using GameScore.Configurations;
using GameScore.Models;
using GameScore.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace GameScore.Controllers
{
    /// <summary>
    /// Player Registration and Authentication
    /// </summary>
    public class AuthController : Controller
    {
        private IAuthenticationService _authService;
        private readonly IPlayerService _playerService;
        private readonly IOptions<PlayerDatabaseSettings> _playerDatabaseSettings;
       
        /// <summary>
        /// Constructor. Parameters come from the dependency injection.
        /// </summary>
        /// <param name="authService">Checks, hashes the password and generates Jwt token</param>
        /// <param name="playerService">Data operations. Internally decides between the cache or the database</param>
        /// <param name="playerDatabaseSettings">Just used to read the pepper here</param>
        public AuthController(
            IAuthenticationService authService,
            IPlayerService playerService,
            IOptions<PlayerDatabaseSettings> playerDatabaseSettings)
        {
            _authService = authService;
            _playerService = playerService;
            _playerDatabaseSettings = playerDatabaseSettings;
        }

        /// <summary>
        /// Registers a new user. Username must be unique.
        /// </summary>
        /// <param name="user">Username and password of the new player</param>
        /// <returns>User name</returns>
        [HttpPost]
        [Route("Register")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        public async Task<IActionResult> Register(LoginUserDto user)
        {
            var player = await _playerService.GetPlayerByNameAsync(user.UserName);
            if (player != null) return BadRequest("Player already exists");

            var generatedSalt = _authService.GenerateSalt(8);
            var hashedPassword = _authService.HashPassword(user.Password, generatedSalt, _playerDatabaseSettings.Value.Pepper);

            player = new Player { Name = user.UserName, Password = hashedPassword, Salt = generatedSalt, Score = 0 };
            await _playerService.CreatePlayerAsync(player);

            return Ok(user.UserName);
        }

        /// <summary>
        /// User Login. Returns a JWT token.
        /// </summary>
        /// <param name="user">Username and password of the registered player</param>
        /// <returns>JWT Token</returns>
        [HttpPost]
        [Route("Login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Login(LoginUserDto user)
        {
            string? token = await _authService.Authenticate(user);
            return string.IsNullOrEmpty(token) ? Unauthorized() : Ok(token);
        }
    }
}
