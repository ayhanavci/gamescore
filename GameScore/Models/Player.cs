﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace GameScore.Models;

/// <summary>
/// Internal Player structure that is saved into the database. Data transfer objects use parts of this class.
/// </summary>
public record Player
{
    /// <summary>
    /// Unique ID. Mongo auto generates this. Has ID index.
    /// </summary>
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; } = string.Empty;
    /// <summary>
    /// Unique player ID. Uniqueness is guaranteed by creating a unique index programatically.
    /// </summary>
    [BsonRequired]    
    [BsonElement("Name")]    
    public string Name { get; set; } = string.Empty;
    /// <summary>
    /// Hashed Password. Salt and Pepper are used when hashing.
    /// </summary>
    public string Password { get; set; } = string.Empty;    
    /// <summary>
    /// Password Salt. Randomly generated for each user.
    /// </summary>
    public string Salt { get; set; } = string.Empty;    
    /// <summary>
    /// Player Game Score. Descending index for fast queries is created programatically.
    /// </summary>
    public int Score { get; set; }
}
