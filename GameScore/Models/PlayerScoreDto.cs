﻿using System.ComponentModel.DataAnnotations;
namespace GameScore.Models;

/// <summary>
/// Data transfer object for player score. Used by LeaderBoardController.
/// </summary>
public record PlayerScoreDto
{
    /// <summary>
    /// Unique name of the player
    /// </summary>
    public string Name { get; set; } = string.Empty;
    /// <summary>
    /// Score of the player.
    /// </summary>
    [Required]
    public int Score { get; set; } = 0;
}
