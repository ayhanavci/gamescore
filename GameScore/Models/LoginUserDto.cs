﻿using System.ComponentModel.DataAnnotations;
namespace GameScore.Models;

/// <summary>
/// Data transfer object used for the authentication.
/// </summary>
public record LoginUserDto
{
    /// <summary>
    /// Unique User Name
    /// </summary>
    [Required]
    public string UserName { get; set; } = String.Empty;
    /// <summary>
    /// Password
    /// </summary>
    [Required]
    public string Password { get; set; } = String.Empty;
}
